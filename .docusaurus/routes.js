import React from "react";
import ComponentCreator from "@docusaurus/ComponentCreator";
import { Auth } from "aws-amplify";
import { Amplify } from "aws-amplify";
import { withAuthenticator } from "@aws-amplify/ui-react";
Amplify.configure({
  Auth: {
    region: "ap-southeast-1",
    userPoolId: "ap-southeast-1_f6tfrmIsS",
    userPoolWebClientId: "1cum79568pe0u434ek1d5kin9a",
    mandatorySignIn: true,
  },
});

const myAppConfig = {
  // ...
  aws_appsync_graphqlEndpoint:
    "https://72tmymo2bzagrcwkmo45j7v5mm.appsync-api.ap-southeast-1.amazonaws.com/graphql",
  aws_appsync_region: "ap-southeast-1",
  aws_appsync_authenticationType: "AMAZON_COGNITO_USER_POOLS", // You have configured Auth with Amazon Cognito User Pool ID and Web Client Id
  // ...
};
Amplify.configure(myAppConfig);
export default [
  {
    path: "/__docusaurus/debug",
    component: withAuthenticator(
      ComponentCreator("/__docusaurus/debug", "275")
    ),
    exact: true,
  },
  {
    path: "/__docusaurus/debug/config",
    component: ComponentCreator("/__docusaurus/debug/config", "b2c"),
    exact: true,
  },
  {
    path: "/__docusaurus/debug/content",
    component: ComponentCreator("/__docusaurus/debug/content", "bce"),
    exact: true,
  },
  {
    path: "/__docusaurus/debug/globalData",
    component: ComponentCreator("/__docusaurus/debug/globalData", "763"),
    exact: true,
  },
  {
    path: "/__docusaurus/debug/metadata",
    component: ComponentCreator("/__docusaurus/debug/metadata", "b13"),
    exact: true,
  },
  {
    path: "/__docusaurus/debug/registry",
    component: ComponentCreator("/__docusaurus/debug/registry", "130"),
    exact: true,
  },
  {
    path: "/__docusaurus/debug/routes",
    component: ComponentCreator("/__docusaurus/debug/routes", "47e"),
    exact: true,
  },
  {
    path: "/blog",
    component: withAuthenticator(ComponentCreator("/blog", "487")),
    exact: true,
  },
  {
    path: "/blog/archive",
    component: withAuthenticator(ComponentCreator("/blog/archive", "d3e")),
    exact: true,
  },
  {
    path: "/blog/first-blog-post",
    component: withAuthenticator(
      ComponentCreator("/blog/first-blog-post", "4a4")
    ),
    exact: true,
  },
  {
    path: "/blog/long-blog-post",
    component: withAuthenticator(
      ComponentCreator("/blog/long-blog-post", "755")
    ),
    exact: true,
  },
  {
    path: "/blog/mdx-blog-post",
    component: withAuthenticator(
      ComponentCreator("/blog/mdx-blog-post", "bad")
    ),
    exact: true,
  },
  {
    path: "/blog/tags",
    component: withAuthenticator(ComponentCreator("/blog/tags", "fc1")),
    exact: true,
  },
  {
    path: "/blog/tags/docusaurus",
    component: withAuthenticator(
      ComponentCreator("/blog/tags/docusaurus", "a6a")
    ),
    exact: true,
  },
  {
    path: "/blog/tags/facebook",
    component: withAuthenticator(
      ComponentCreator("/blog/tags/facebook", "aaa")
    ),
    exact: true,
  },
  {
    path: "/blog/tags/hello",
    component: withAuthenticator(ComponentCreator("/blog/tags/hello", "28b")),
    exact: true,
  },
  {
    path: "/blog/tags/hola",
    component: withAuthenticator(ComponentCreator("/blog/tags/hola", "720")),
    exact: true,
  },
  {
    path: "/blog/welcome",
    component: withAuthenticator(ComponentCreator("/blog/welcome", "782")),
    exact: true,
  },
  {
    path: "/markdown-page",
    component: withAuthenticator(ComponentCreator("/markdown-page", "4da")),
    exact: true,
  },
  {
    path: "/docs",
    component: withAuthenticator(ComponentCreator("/docs", "b26")),
    routes: [
      {
        path: "/docs/category/tutorial---basics",
        component: withAuthenticator(
          ComponentCreator("/docs/category/tutorial---basics", "d44")
        ),
        exact: true,
        sidebar: "tutorialSidebar",
      },
      {
        path: "/docs/category/tutorial---extras",
        component: withAuthenticator(
          ComponentCreator("/docs/category/tutorial---extras", "f09")
        ),
        exact: true,
        sidebar: "tutorialSidebar",
      },
      {
        path: "/docs/intro",
        component: withAuthenticator(ComponentCreator("/docs/intro", "aed")),
        exact: true,
        sidebar: "tutorialSidebar",
      },
      {
        path: "/docs/tutorial-basics/congratulations",
        component: withAuthenticator(
          ComponentCreator("/docs/tutorial-basics/congratulations", "793")
        ),
        exact: true,
        sidebar: "tutorialSidebar",
      },
      {
        path: "/docs/tutorial-basics/create-a-blog-post",
        component: withAuthenticator(
          ComponentCreator("/docs/tutorial-basics/create-a-blog-post", "68e")
        ),
        exact: true,
        sidebar: "tutorialSidebar",
      },
      {
        path: "/docs/tutorial-basics/create-a-document",
        component: withAuthenticator(
          ComponentCreator("/docs/tutorial-basics/create-a-document", "c2d")
        ),
        exact: true,
        sidebar: "tutorialSidebar",
      },
      {
        path: "/docs/tutorial-basics/create-a-page",
        component: withAuthenticator(
          ComponentCreator("/docs/tutorial-basics/create-a-page", "f44")
        ),
        exact: true,
        sidebar: "tutorialSidebar",
      },
      {
        path: "/docs/tutorial-basics/deploy-your-site",
        component: withAuthenticator(
          ComponentCreator("/docs/tutorial-basics/deploy-your-site", "e46")
        ),
        exact: true,
        sidebar: "tutorialSidebar",
      },
      {
        path: "/docs/tutorial-basics/markdown-features",
        component: withAuthenticator(
          ComponentCreator("/docs/tutorial-basics/markdown-features", "4b7")
        ),
        exact: true,
        sidebar: "tutorialSidebar",
      },
      {
        path: "/docs/tutorial-extras/manage-docs-versions",
        component: withAuthenticator(
          ComponentCreator("/docs/tutorial-extras/manage-docs-versions", "fdd")
        ),
        exact: true,
        sidebar: "tutorialSidebar",
      },
      {
        path: "/docs/tutorial-extras/translate-your-site",
        component: withAuthenticator(
          ComponentCreator("/docs/tutorial-extras/translate-your-site", "2d7")
        ),
        exact: true,
        sidebar: "tutorialSidebar",
      },
    ],
  },
  {
    path: "/",
    component: withAuthenticator(ComponentCreator("/", "5f0")),
    exact: true,
  },
  {
    path: "*",
    component: ComponentCreator("*"),
  },
];
